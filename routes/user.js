const express = require("express");
const router = express.Router();
const User = require("../models/User");
const UserController = require("../controllers/user");

// Rest
// router.post("/", (req, res) => {
//   let user = new User({
//     firstName: req.body.firstName,
//     lastName: req.body.lastName,
//     email: req.body.email,
//     mobileNumber: req.body.mobileNumber,
//     password: req.body.password,
//     loginType: "email",
//   });

//   user.save().then((user, err) => {
//     res.send(err ? false : true);
//   });
// });

router.post("/", (req, res) => {
  UserController.register(req.body).then((result) => res.send(result));
});

router.post("/email-exists", (req, res) => {});

router.post("login", (req, res) => {});

module.exports = router;
