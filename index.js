const express = require("express");
const app = express();
const mongoose = require("mongoose");
const userRoutes = require("./routes/user");
const { ApolloServer } = require("apollo-server-express");

app.use(express.json());
app.use(express.urlencoded({ extended: true })); //Generates readable URL

app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online on port ${process.env.PORT || 4000}`);
  // julius back tick
});

mongoose.connection.once("open", () => {
  console.log("Now connected to local MongoDB server");
});

// local MongoDB connection
// mongoose.connect("mongodb://localhost:27017/course_booking_demo", {
//   useNewURLParser: true,
//   useUnifiedTopology: true,
// });

//online MongoDB connection
mongoose.connect(
  "mongodb+srv://jino:jino123@cluster1-7j4kj.mongodb.net/men_g?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

new ApolloServer({
  typeDefs: require("./graphql/schema"),
  resolvers: require("./graphql/resolvers"),
}).applyMiddleware({ app, path: "/graphql" });

//connect route files
//used to distinguish between frontend and backend routes
app.use("/api/users", userRoutes);
