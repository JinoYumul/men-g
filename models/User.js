const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First name is required."],
  },
  lastName: {
    type: String,
    required: [true, "Last name is required."],
  },
  email: {
    type: String,
    required: [true, "Email is required."],
  },
  loginType: {
    type: String,
    required: [true, "Login type is required"],
  },
  password: {
    type: String,
  },
  mobileNumber: {
    type: String,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  enrollments: [
    {
      courseId: {
        type: String,
        required: [true, "Course ID is required."],
      },
      enrolledoN: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: "Enrolled", //alternative values are cancelled and completed
      },
    },
  ],
});

module.exports = mongoose.model("user", userSchema);
