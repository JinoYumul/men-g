// const User = require("../models/User");
const UserController = require("../controllers/user");

// module.exports = {
//   Mutation: {
//     register: (_, args) => {
//       let user = new User({
//         firstName: args.firstName,
//         lastName: args.lastName,
//         email: args.email,
//         mobileNumber: args.mobileNumber,
//         password: args.password,
//         loginType: "email",
//       });

//       return user.save().then((user, err) => {
//         return err ? false : true;
//       });
//     },
//   },
// };

module.exports = {
  Mutation: {
    register: (_, args) => {
      return UserController.register(args);
    },
  },
};
