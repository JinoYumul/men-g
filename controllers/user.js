const User = require("../models/User");

module.exports.register = (params) => {
  let user = new User({
    firstName: params.firstName,
    lastName: params.lastName,
    email: params.email,
    mobileNumber: params.mobileNumber,
    password: params.password,
    loginType: "email",
  });

  return user.save().then((user, err) => {
    return err ? false : true;
  });
};
